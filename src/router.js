import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Profile from './views/Profile.vue'
import Signup from './views/Signup.vue'
import Articles from './views/Articles.vue'
import Article from './views/Article.vue'
import ArticleForm from './views/ArticleForm.vue'
import Adoptions from './views/Adoptions.vue'
import AdoptionForm from './views/AdoptionForm.vue'
import Adoption from './views/Adoption.vue'
import Roles from './views/Roles.vue'
import Users from './views/Users.vue'
import error404 from './views/error404.vue'

Vue.use(Router);

let route = new Router({
    mode: process.env.VUE_APP_ROUTE_MODE,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: {'active': 'home', 'title':'Inicio'},
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {'active': 'myprofile', 'title':'Login'},
        },
        {
            path: '/myprofile',
            name: 'myprofile',
            component: Profile,
            meta: {'active': 'myprofile', 'title':'Mi usuario'},
        },
        {
            path: '/myprofile/edit',
                name: 'edit my profile',
            component: Signup,
            meta: {'active': 'myprofile', 'title':'Editar perfil'},
        },
        {
            path: '/profile/:id',
            name: 'profile',
            component: Profile,
            meta: {'active': 'users', 'title':'Perfil'},
        },
        {
            path: '/signup',
            name: 'signup',
            component: Signup,
            props: true, //Para el acceso con $route pasando parametros
            meta: {'active': 'myprofile', 'title':'Registro'},
        },
        {
            path: '/profile/edit/:id',
            name: 'edit',
            component: Signup,
            meta: {'active': 'users', 'title':'Editar perfil'},
        },
        {
            path: '/articles/',
            name: 'articles',
            component: Articles,
            meta: {'active': 'article', 'title':'Artículos'},
        },
        {
            path: '/articles/tag/:tag',
            name: 'search articles by tag',
            component: Articles,
            meta: {'active': 'article', 'title':'Busqueda por tag'},
        },
        {
            path: '/article/new',
            name: 'new article',
            component: ArticleForm,
            meta: {'active': 'article', 'title':'Nuevo artículo'},
        },
        {
            path: '/article/edit/:id',
            name: 'edit article',
            component: ArticleForm,
            meta: {'active': 'article', 'title':'Editar artículo'},
        },
        {
            path: '/article/:id',
            name: 'article',
            component: Article,
            meta: {'active': 'article', 'title':''},
        },
        {
            path: '/adoptions/',
            name: 'adoptions',
            component: Adoptions,
            meta: {'active': 'adoption', 'title':'Listado de adopciones'},
        },
        {
            path: '/adoption/new',
            name: 'new adoption',
            component: AdoptionForm,
            meta: {'active': 'adoption', 'title':'Nueva ficha de adopción'},
        },
        {
            path: '/adoption/edit/:id',
            name: 'edit adoption',
            component: AdoptionForm,
            meta: {'active': 'adoption', 'title':'Editar ficha'},
        },
        {
            path: '/adoption/:id',
            name: 'adoption',
            component: Adoption,
            meta: {'active': 'adoption', 'title':'Ficha'},
        },
        {
            path: '/roles/',
            name: 'roles',
            component: Roles,
            meta: {'active': 'role', 'title':'Administrar roles'},
        },
        {
            path: '/users/',
            name: 'users',
            component: Users,
            meta: {'active': 'users', 'title':'Listado de usuarios'},
        },
        {
            path: '*',
            name: '404',
            component: error404,
            meta: {'active': null, 'title':'Error 404'},
        }


    ]
});


route.beforeEach((to, from, next) => {
    document.title = process.env.VUE_APP_WEB_TITLE + ' - '+to.meta.title;
    next();
});

export default route;
