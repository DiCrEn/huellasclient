import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VTooltip from 'v-tooltip'
import wysiwyg from "vue-wysiwyg";


Vue.config.productionTip = true;

const URLAPI = process.env.VUE_APP_BASE_API_URI;

Vue.mixin({
    data: function() {
        return {
            get WEBNAME() { return process.env.VUE_APP_WEB_TITLE },
            get URLAPI() {
                return URLAPI;
            },
            get ROLE_LIST() {
                return {
                    article: {
                        w: parseInt("1",2), //Crear post y editar el mio
                        e: parseInt("10",2), //Editar el de los demás
                    },
                    adoption: {
                        w: parseInt("100",2), //Crear ficha y editar la mia
                        e: parseInt("1000",2), //Editar el de los demás
                    },
                    comment: {
                        w: parseInt("10000",2), //Crear comentario y editar el mio
                        e: parseInt("100000",2), //Editar el de los demás
                    },
                    user: {
                        r: parseInt("1000000",2), //Leer datos sensibles de los usuarios
                        e: parseInt("10000000",2), //Se asume que puede editar el de los demás.
                    },
                    role: {
                        w: parseInt("100000000",2), //Puede crear editar y eliminar roles.
                    },
                }
            },
        }
    },
    methods: {
        logout: function(){
            store.commit("logout");
            //EventBus.$emit('onlogout'); DEPRECATED. Usar VUEX
        },
        request: function(endpoint,type = "get",data){
            let headers = {'content-type': 'application/json'};
            if(store.state.user.logged){
                headers.Authorization = store.state.user.token;
            }

            return axios({
                method: type,
                headers: headers,
                data: type.toUpperCase() !== 'GET' ? data : undefined,
                params: type.toUpperCase() === 'GET' ? data : undefined,
                url: URLAPI + endpoint,
                json: true,
            });
        },
        requestAll: function(array){
            return axios.all(array);
        },
        filerequest: async function(endpoint,formdata){
            try {
                const response = await axios.post(URLAPI + endpoint, formdata, {
                    headers: {
                        Authorization: store.state.user.token,
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
                });
                return response;
            }catch(e){
                throw e;
            }

        },
        avatar(profile){
            if(profile._id || profile.id) {
                const profileid = profile._id ? profile._id : profile.id;
                return profile.image === undefined || profile.image === '' ? 'https://robohash.org/set_set4/bgset_bg1/' + profileid + '?size=80x80'
                    : URLAPI + 'user/avatar/' + profile.image;
            }else{
                return "";
            }
        },
        datetime: function(txt,conf){
            const date = new Date(txt);
            if(conf === 'date'){
                return date.toLocaleDateString();
            }else if(conf === 'today'){
                const today = new Date();
                const hours = Math.abs(today - date) / 36e5;
                if(hours < 24){
                    return date.toLocaleTimeString();
                }else{
                    return date.toLocaleDateString()+" "+date.toLocaleTimeString();
                }
            }else if(conf === 'today or date'){
                const today = new Date();
                const hours = Math.abs(today - date) / 36e5;
                if(hours < 24){
                    return date.toLocaleTimeString();
                }else{
                    return date.toLocaleDateString();
                }
            }else{
                return date.toLocaleDateString()+" "+date.toLocaleTimeString();
            }

        },
        safeText: function(text){
            //Ya no lo necesito, lo hago al guardar en el servidor. Pero dejo esto aquí por si acaso.
            return text;
        }
    },
    computed: {

    }
});

Vue.use(VTooltip);

Vue.use(wysiwyg, {
    iconOverrides: {
        "bold": '<i class="fas fa-bold"></i>',
        "italic": '<i class="fas fa-italic"></i>',
        "underline": '<i class="fas fa-underline"></i>',
        "justifyLeft": '<i class="fas fa-align-left"></i>',
        "justifyCenter": '<i class="fas fa-align-center"></i>',
        "justifyRight": '<i class="fas fa-align-right"></i>',
        "headings": '<i class="fas fa-heading"></i>',
        "link": '<i class="fas fa-link"></i>',
        "code": '<i class="fas fa-code"></i>',
        "orderedList": '<i class="fas fa-list-ol"></i>',
        "unorderedList": '<i class="fas fa-list-ul"></i>',
        "image": '<i class="fas fa-images"></i>',
        "table": '<i class="fas fa-table"></i>',
        "removeFormat": '<i class="fas fa-eraser"></i>',
    }
});


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
