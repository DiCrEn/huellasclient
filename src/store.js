import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex);

//https://alligator.io/vuejs/vuex-persist-state/
const vuexLocalStorage = new VuexPersist({
    key: 'vuex',// The key to store the state on in the storage provider.
    storage: window.localStorage, // or window.sessionStorage or localForage
});

export default new Vuex.Store({
    state: {
        user: {
            name: '',
            lastname: '',
            logged: false,
            token: '',
            id: '',
            email: '',
            image: '',
            permissions: 0
        }
    },
    plugins: [vuexLocalStorage.plugin],
    mutations: { //Para modificar los states. Usar Vue.set para que reaccione al cambiar propiedades de objetos
        logout: function(state){
            state.user.token = null;
            state.user.logged = false;
            state.user.id = '';
            state.user.email = '';
            state.user.image = '';
            state.user.name = '';
            state.user.lastname = '';
            state.user.permissions = 0;
        },
        setUser: function(state, user){
            state.user = user;
        }
    },
    actions: { //Para datos asincronos

    }
})
